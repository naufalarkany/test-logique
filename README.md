# LOGIQUE

> TEST LOGIQUE

## About

This project uses for tech test Logique

## Getting Started

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

   ```
   npm install
   ```

3. Create your ENV base on .env.example

4. Create and migrate db

   ```
   npx sequelize db:create
   ```

   ```
   npx sequelize db:migrate
   ```

5. Start your app

   ```
   npm start
   ```
