const { BadRequestError } = require("../errors");
const creditCardValidator = require("card-validator");
const validateRequest = (reqBody, schema) => {
  const { error, value } = schema.validate(reqBody, { abortEarly: false });

  if (error) {
    const errorMessages = error.details.map((err) => err.message);
    throw new BadRequestError("Validation Error", errorMessages);
  } else {
    return value;
  }
};

const isValidCreditCard = (value, helpers) => {
  const isValid = creditCardValidator.number(value).isValid;
  console.log(value);
  console.log(isValid);
  if (!isValid) {
    return helpers.error("credit cart not valid");
  }

  return value;
};

const isValidExpiredDate = (value, helpers) => {
  const parts = value.split("/");
  const expMonth = parseInt(parts[0], 10);
  const expYear = parseInt(parts[1], 10);

  const currentDate = new Date();
  const currentYear = currentDate.getFullYear() % 100;
  const currentMonth = currentDate.getMonth() + 1;

  if (expYear < currentYear || (expYear === currentYear && expMonth < currentMonth)) {
    return helpers.error("Credit is expired");
  }

  return value;
};

const isValidCVV = (value, helpers) => {
  const isValid = creditCardValidator.cvv(value).isValid;

  if (!isValid) {
    return helpers.error("CVV tidak valid");
  }

  return value;
};
module.exports = {
  validateRequest,
  isValidCreditCard,
  isValidExpiredDate,
  isValidCVV,
};
