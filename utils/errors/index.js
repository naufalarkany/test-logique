const BadRequestError = require("./badRequestError");
const UnauthorizedError = require("./unauthorizedError");
const UnauthenticatedError = require("./unauthenticatedError");
const NotFounError = require("./notFoundError");

module.exports = {
  BadRequestError,
  UnauthorizedError,
  UnauthenticatedError,
  NotFounError,
};
