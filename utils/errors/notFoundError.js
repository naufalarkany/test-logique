const CustomError = require("./customError");
class BadRequest extends CustomError {
  constructor(message, errors) {
    super(message);
    this.statusCode = 400;
    this.data = errors;
  }
}

module.exports = BadRequest;
