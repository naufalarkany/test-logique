const CustomError = require("./customError");
class UnauthorizedError extends CustomError {
  constructor() {
    super();
    this.statusCode = 401;
    this.message = "Invalid API Key.";
  }
}

module.exports = UnauthorizedError;
