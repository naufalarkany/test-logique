const CustomError = require("./customError");
class UnauthenticatedError extends CustomError {
  constructor() {
    super();
    this.statusCode = 403;
    this.message = "API key is missing.";
  }
}

module.exports = UnauthenticatedError;
