const { CRYPTO_SECRET_KEY } = process.env;
const CryptoJS = require("crypto-js");

const encrypt = (text) => {
  return CryptoJS.AES.encrypt(text, CRYPTO_SECRET_KEY).toString();
};

const decrypt = (text) => {
  const bytes = CryptoJS.AES.decrypt(text, CRYPTO_SECRET_KEY);
  return bytes.toString(CryptoJS.enc.Utf8);
};
module.exports = {
  encrypt,
  decrypt,
};
