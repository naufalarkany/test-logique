const { Model } = require("sequelize");
const { decrypt, encrypt } = require("../utils/encryption");
module.exports = (sequelize, DataTypes) => {
  class UserCreditCard extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserCreditCard.belongsTo(models.User, {
        foreignKey: {
          name: "user_id",
        },
      });
    }
  }
  UserCreditCard.init(
    {
      user_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      type: DataTypes.STRING,
      number: {
        type: DataTypes.STRING,
        set(value) {
          this.setDataValue("number", encrypt(value));
        },
        get() {
          const value = this.getDataValue("number");
          return value ? decrypt(value) : null;
        },
      },
      expired: {
        type: DataTypes.STRING,
        set(value) {
          this.setDataValue("expired", encrypt(value));
        },
        get() {
          const value = this.getDataValue("expired");
          return value ? decrypt(value) : null;
        },
      },
      cvv: {
        type: DataTypes.STRING,
        set(value) {
          this.setDataValue("cvv", encrypt(value));
        },
        get() {
          const value = this.getDataValue("cvv");
          return value ? decrypt(value) : null;
        },
      },
    },
    {
      sequelize,
      modelName: "UserCreditCard",
    }
  );
  return UserCreditCard;
};
