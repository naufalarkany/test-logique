const jwt = require("jsonwebtoken");
const { UnauthorizedError, UnauthenticatedError } = require("../utils/errors");
const { User } = require("../models");

const authenticationMiddleware = async (req, res, next) => {
  try {
    const authHeader = req.headers.key;
    if (!authHeader) throw new UnauthenticatedError();
    if (authHeader !== process.env.API_KEY) throw new UnauthorizedError();

    return next();
  } catch (error) {
    next(error);
  }
};

module.exports = authenticationMiddleware;
