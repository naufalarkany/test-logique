const invalidPathHandler = (req, res, next) => {
  return res.status(404).send({
    status: "failed",
    msg: "Routes Not Found",
  });
};

module.exports = invalidPathHandler;
