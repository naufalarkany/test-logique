const errorHandler = (err, req, res, next) => {
  return res.status(err.statusCode || 500).send({
    status: "failed",
    msg: err.message || "Something went wrong. Please try again later.",
    errors: err.data,
  });
};

module.exports = errorHandler;
