const Joi = require("joi");
const { Op } = require("sequelize");
const { User, UserCreditCard, sequelize } = require("../models");
const {
  validateRequest,
  isValidCreditCard,
  isValidExpiredDate,
  isValidCVV,
} = require("../utils/validation");
const { NotFounError, BadRequestError } = require("../utils/errors");
const regexExpiredDate = /^(0[1-9]|1[0-2])\/?([0-9]{2})$/;
const regexCVV = /^[0-9]{3,4}$/;
exports.create = async (req, res, next) => {
  const userRegisterScheme = Joi.object({
    name: Joi.string().required(),
    address: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(8).required(),
    creditcard_name: Joi.string().required(),
    creditcard_type: Joi.string().required(),
    creditcard_number: Joi.string().required().custom(isValidCreditCard, "custom validation"),
    creditcard_expired: Joi.string()
      .required()
      .pattern(regexExpiredDate)
      .custom(isValidExpiredDate, "custom validation"),
    creditcard_cvv: Joi.string()
      .required()
      .pattern(regexCVV)
      .custom(isValidCVV, "custom validation"),
  });
  const t = await sequelize.transaction();
  try {
    const {
      name,
      address,
      email,
      password,
      creditcard_type,
      creditcard_name,
      creditcard_number,
      creditcard_expired,
      creditcard_cvv,
    } = validateRequest(req.body, userRegisterScheme);
    const newUser = {
      name,
      address,
      email,
      password,
    };
    const user = await User.findOne({
      where: {
        email,
      },
    });
    if (user) throw new BadRequestError("Email already registered");
    const createdUser = await User.create(newUser, { transaction: t });

    await UserCreditCard.create(
      {
        user_id: createdUser.id,
        name: creditcard_name,
        type: creditcard_type,
        number: creditcard_number,
        expired: creditcard_expired,
        cvv: creditcard_cvv,
      },
      { transaction: t }
    );
    await t.commit();
    return res.status(201).send({
      status: "success",
      msg: "User Successfully Created",
      data: createdUser,
    });
  } catch (error) {
    await t.rollback();
    next(error);
  }
};

exports.getAll = async (req, res, next) => {
  try {
    const { q, ob = "name", sb = "asc", lt = 30, of = 0 } = req.query;
    const { count, rows } = await User.findAndCountAll({
      limit: Number(lt),
      offset: Number(of),
      where: {
        ...(q && {
          [Op.or]: [
            {
              name: {
                [Op.iLike]: `%${q}%`,
              },
            },
            {
              address: {
                [Op.iLike]: `%${q}%`,
              },
            },
            {
              email: {
                [Op.iLike]: `%${q}%`,
              },
            },
          ],
        }),
      },
      attributes: {
        exclude: ["createdAt", "updatedAt", "id", "password"],
        include: [["id", "user_id"]],
      },
      include: [
        {
          model: UserCreditCard,
          as: "creditcard",
          attributes: {
            exclude: ["createdAt", "updatedAt", "id", "user_id"],
          },
        },
      ],
      order: [[ob, sb]],
    });
    return res.status(200).send({
      status: "success",
      msg: "Successfully Get Users",
      data: {
        count,
        rows,
      },
    });
  } catch (error) {
    next(error);
  }
};

exports.getById = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const user = await User.findByPk(userId, {
      attributes: {
        exclude: ["createdAt", "updatedAt", "id", "password"],
        include: [["id", "user_id"]],
      },
      include: [
        {
          model: UserCreditCard,
          as: "creditcard",
          attributes: {
            exclude: ["createdAt", "updatedAt", "id", "user_id"],
          },
        },
      ],
    });
    if (!user) throw new NotFounError("User Not Found");
    return res.status(200).send({
      status: "success",
      msg: "Successfully Get User",
      data: user,
    });
  } catch (error) {
    next(error);
  }
};

exports.update = async (req, res, next) => {
  const userScheme = Joi.object({
    user_id: Joi.number().required(),
    name: Joi.string(),
    address: Joi.string(),
    email: Joi.string().email(),
    password: Joi.string().min(8),
    creditcard_type: Joi.string(),
    creditcard_name: Joi.string(),
    creditcard_number: Joi.string().custom(isValidCreditCard, "custom"),
    creditcard_expired: Joi.string()
      .pattern(regexExpiredDate)
      .custom(isValidExpiredDate, "custom validation"),
    creditcard_cvv: Joi.string().pattern(regexCVV).custom(isValidCVV, "custom validation"),
  });
  const t = await sequelize.transaction();

  try {
    const data = validateRequest(req.body, userScheme);
    const userId = data.user_id;
    delete data.user_id;
    const user = await User.findByPk(userId);
    if (!user) throw new NotFounError("User Not Found");
    if (
      data.creditcard_number ||
      data.creditcard_expired ||
      data.creditcard_cvv ||
      data.creditcard_name ||
      data.creditcard_type
    ) {
      const {
        creditcard_number,
        creditcard_expired,
        creditcard_cvv,
        creditcard_type,
        creditcard_name,
      } = data;
      const updatedCreditCard = {
        ...(creditcard_number && { number: creditcard_number }),
        ...(creditcard_expired && { expired: creditcard_expired }),
        ...(creditcard_cvv && { cvv: creditcard_cvv }),
        ...(creditcard_type && { type: creditcard_type }),
        ...(creditcard_name && { name: creditcard_name }),
      };
      await UserCreditCard.update(updatedCreditCard, { where: { user_id: userId } });
      delete data.creditcard_number;
      delete data.creditcard_expired;
      delete data.creditcard_cvv;
      delete data.creditcard_type;
      delete data.creditcard_name;
    }

    await user.update(data);
    return res.status(200).send({
      status: "success",
      msg: "User Successfully Updated",
      data: { status: true },
    });
  } catch (error) {
    next(error);
  }
};
