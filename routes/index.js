const { Router } = require("express");
const authenticationMiddleware = require("../middleware/authentication");
const userRouter = require("./userRoutes");
const router = Router();

router.use("/user", authenticationMiddleware, userRouter);

module.exports = router;
