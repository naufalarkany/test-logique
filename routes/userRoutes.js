const { Router } = require("express");
const { user } = require("../controller");
const router = Router();

router.post("/register", user.create);
router.get("/list", user.getAll);
router.get("/:userId", user.getById);
router.patch("/", user.update);
module.exports = router;
